This website has been designed for an iPhone 7 as a web app. It is compatible with any 6" phone, and will work with many common web browsers however for the best experience, this site should be viewed at 357x667px. 

Assumptions made. 
The site is designed as a prototype only! and has limited functionality through JS, functionality mainly affected include, 
    - usernames, passwords and profile information, 
    - money raised, 
    - donations being saved,

The next prototype will improve functionality though the use of SQL and PHP, allowing information to be stored in a database and accessed.

All calendar and news feeds link to online services, Twitter and Google. This was decided to allow the organisation to create a profile which would allow for little administration, whilst offering a good product.  


